#!/bin/bash
############
docker login -u u217 -p P@ssw0rd123
docker build -f  Docker/Dockerfile -t u217/devops-practices:$(sh setversion) .
docker push u217/devops-practices:$(sh setversion)

if [ "$DATABASE"  = "h2" ]; then
echo "run h2 java app"
export APP_IMAGE_TAG=$(sh setversion)
docker run -id -e DATABASE=$DATABASE -p 8090:8090 --name=app-devops-practices u217/devops-practices:$(sh setversion)
fi

if [ "$DATABASE"  = "mysql" ]; then
echo "run mysql java app"
export APP_IMAGE_TAG=$(sh setversion)
cd Docker-Compose
docker-compose up -d
fi

echo "\033[0;36m===========================================================\033[0;36m"
echo "\033[0;36mStart Person User testing\033[0;36m"
echo "\033[0;36m===========================================================\033[0;36m"

TESTUSERNAME=$APP_IMAGE_TAG

sleep 40
USERCREATION=$(
curl -X POST \
  http://localhost:8090/persons/ \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
   "name": "'"$TESTUSERNAME"'",
   "age": 41
}')

echo $USERCREATION

if [ "$USERCREATION" = "$TESTUSERNAME" ]; then
echo "\033[0;36m===========================================================\033[0;36m"
echo "\033[0;32m Run Testing Successfully\033[0;36m"
echo "\033[0;36m===========================================================\033[0;36m"
else
echo "\033[0;31m===========================================================\033[0;31m"
echo "\033[0;31m Run Testing Failed\033[0m"  
echo "\033[0;31m===========================================================\033[0;31m"
exit 1
fi

echo "\033[0;36m===========================================================\033[0;36m"
echo "\033[0;36m finished Person User testing\033[0;36m"
echo "\033[0;36m===========================================================\033[0;36m"