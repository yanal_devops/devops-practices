#!/bin/bash
############
if [ "$DATABASE"  = "h2" ]; then
echo "run h2 java app"
java -jar -Dserver.port=8090 -Dspring.profiles.active=$DATABASE /app/assignment.jar
fi

if [ "$DATABASE"  = "mysql" ]; then
echo "run mysql java app"
java -jar -Dserver.port=8090 -Dspring.datasource.username=$DB_USERNAME -Dspring.datasource.password=$DB_PASSWORD -Dspring.datasource.url=$DB_URL /app/assignment.jar
fi