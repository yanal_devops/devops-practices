#!/bin/bash

CHK_SRV_START=$(ps -ef | grep target/assignment | grep -v grep | wc -l)
CHK_SQL_START=$(docker ps -q --filter "name=mysql-devops-practices" | wc -l)
CHK_APP_START=$(docker ps -q --filter "name=app-devops-practices" | wc -l)

if [ $CHK_SQL_START -eq 1 ]; then
echo =======================================================
echo Remove running SQL container
echo =======================================================
for X in $(docker ps -q --filter "name=mysql-devops-practices" | grep -v grep | cut  -c 1-20); do docker rm --force $X; done;
fi

if [ $CHK_APP_START -eq 1 ]; then
echo =======================================================
echo Remove running APP container
echo =======================================================
for X in $(docker ps -q --filter "name=app-devops-practices" | grep -v grep | cut  -c 1-20); do docker rm --force $X; done;
fi

if [ $CHK_SRV_START -eq 1 ]; then
echo =======================================================
echo Kill service
echo =======================================================
for X in $(ps -ef | grep target/assignment | grep -v grep | cut  -c 10-15); do kill -9 $X; done;
fi

echo =======================================================
echo start service
echo =======================================================
java -Dserver.port=8090 -jar -Dspring.profiles.active=h2 target/*.jar &

sleep 40

echo "\033[0;36m===========================================================\033[0;36m"
echo "\033[0;36mStart Person User testing\033[0;36m"
echo "\033[0;36m===========================================================\033[0;36m"

USERCREATION=$(
curl -X POST \
  http://localhost:8090/persons/ \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
   "name": "TEST",
   "age": 41
}')

for X in $(ps -ef | grep target/assignment | grep -v grep | cut  -c 10-15); do kill -9 $X; done;

sleep 2
echo $USERCREATION

if [ "$USERCREATION" = "TEST" ]; then
echo "\033[0;36m===========================================================\033[0;36m"
echo "\033[0;32m Run Testing Successfully\033[0;36m"
echo "\033[0;36m===========================================================\033[0;36m"
else
echo "\033[0;31m===========================================================\033[0;31m"
echo "\033[0;31m Run Testing Failed\033[0m"  
echo "\033[0;31m===========================================================\033[0;31m"
exit 1
fi

echo "\033[0;36m===========================================================\033[0;36m"
echo "\033[0;36m finished Person User testing\033[0;36m"
echo "\033[0;36m===========================================================\033[0;36m"


echo "done"